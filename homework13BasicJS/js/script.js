let imagesArr = ['./img/1.jpg', './img/2.jpg', './img/3.jpg', './img/4.png'];

let img = document.querySelector('.image-to-show'),
  i = 0,
  timerId;

if (img) {
  let storageI = localStorage.getItem('stopI');
  if (storageI) {
    i = storageI;
  }

  img.src = imagesArr[i];
  addStopBtn(img.closest('div'));
  setCounter(img.closest('div'), 3);
  timerId = setTimeout(changeImage, 3000);
}

function changeImage() {
  if (i < imagesArr.length - 1) {
    i++;
  } else {
    i = 0;
  }
  img.src = imagesArr[i];

  setCounter(img.closest('div'), 3);

  timerId = setTimeout(changeImage, 3000);
}

function addStopBtn(parent) {
  if (document.querySelector('.btn-stop')) return;

  let stopBtn = document.createElement('button');
  stopBtn.className = 'btn-stop';
  stopBtn.innerText = 'стоп';
  stopBtn.addEventListener('click', (event) => {
    clearTimeout(timerId);
    localStorage.setItem('stopI', i);

    addContinueBtn(event.target);
  });

  parent.after(stopBtn);
}

function addContinueBtn(stopBtn) {
  if (document.querySelector('.btn-continue')) return;
  let continueBtn = document.createElement('button');
  continueBtn.className = 'btn-continue';
  continueBtn.innerText = 'продолжить';

  continueBtn.addEventListener('click', (ev) => {
    i = localStorage['stopI'] || 0;

    timerId = setTimeout(changeImage, 3000);
    ev.target.remove();
  });
  stopBtn.after(continueBtn);
}

function setCounter(parentBlock, seconds) {
  seconds = seconds || 0;

  let oldCounter = document.querySelector('.counter'),
    counter = document.createElement('span');
  if (oldCounter) {
    oldCounter.remove();
  }

  counter.className = 'counter';
  counter.innerText = seconds;
  parentBlock.insertAdjacentElement('afterbegin', counter);

  if (seconds > 0) {
    let intervalOut = setInterval(function counterLower() {
      let currentNumber = counter.innerText;

      if (!isNaN(currentNumber) && +currentNumber > 0) {
        counter.innerText = --currentNumber;
      } else {
        clearInterval(intervalOut);
      }
    }, 1000);
  }
}
