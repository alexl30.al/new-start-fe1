let keyButtons = document.querySelectorAll('.btn'),
  activeButton;

if (keyButtons) {
  keyButtons = [...keyButtons];
}

document.body.addEventListener('keydown', (event) => {
  activeButton = document.querySelector('.btn-active');
  if (activeButton) {
    activeButton.classList.remove('btn-active');
  }

  keyButtons.map((btn) => {
    if (
      event.key === btn.innerText.toLowerCase() ||
      event.code === btn.innerText
    ) {
      btn.classList.add('btn-active');
    }
  });
});
