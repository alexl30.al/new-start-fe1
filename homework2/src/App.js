import "./App.css";
// import React, { useState, useEffect } from "react";
import React, { Component } from "react";
import axios from "axios";

import Cart from "./components/Cart";
import FavoriteList from "./components/FavoriteList";
import ProductCard from "./components/ProductCard";

class App extends Component {
  state = {
    productList: [],
    productsOnCart: JSON.parse(localStorage.getItem("cart")) || [], //new [] of {}
    productsInFavorite: JSON.parse(localStorage.getItem("favorites")) || [], //new [] of {}
  };

  //old
  setProductList = (productList) => {
    this.setState({
      productList: productList,
    });
  };

  //new function
  manageFavorites = (articul) => {
    const { productsInFavorite } = this.state;
    const isFound = this.checkInFavorites(articul);

    if (isFound) {
      this.setState(
        {
          productsInFavorite: productsInFavorite.filter(
            (art) => art !== articul
          ),
        },
        () => {
          localStorage.setItem(
            "favorites",
            JSON.stringify(this.state.productsInFavorite)
          );
        }
      );
    } else {
      productsInFavorite.push(articul);
      this.setState({ productsInFavorite }, () => {
        localStorage.setItem(
          "favorites",
          JSON.stringify(this.state.productsInFavorite)
        );
      });
    }
  };

  //new function
  manageCart = (productInfo) => {
    const { productsOnCart } = this.state;
    const isFound = this.ckeckInCart(productInfo.articul);

    //del from cart
    if (isFound) {
      this.setState(
        {
          productsOnCart: productsOnCart.filter(
            (product) => product.articul !== productInfo.articul
          ),
        },
        () => {
          localStorage.setItem(
            "cart",
            JSON.stringify(this.state.productsOnCart)
          );
        }
      );
    } else {
      productsOnCart.push(productInfo);
      this.setState({ productsOnCart }, () => {
        localStorage.setItem("cart", JSON.stringify(this.state.productsOnCart));
      });
    }
  };

  ckeckInCart = (articul) => {
    return !!this.state.productsOnCart.find(
      (product) => product.articul === articul
    );
  };

  checkInFavorites = (articul) => {
    return !!this.state.productsInFavorite.find((art) => art === articul);
  };

  componentDidMount() {
    axios("/products.json") //запрос в файл в папку /public
      .then((response) => {
        this.setProductList(response.data);
      })
      .catch((err) => console.log("error", err));
  }

  render() {
    const { productList, productsOnCart, productsInFavorite } = this.state;
    return (
      <>
        <Cart
          counter={productsOnCart.length}
          sum={
            productsOnCart.length > 0
              ? productsOnCart.reduce((acc, cur) => acc + cur.price, 0)
              : 0
          }
        />
        <FavoriteList counter={productsInFavorite.length > 0 ? productsInFavorite.length : 0} />

        {productList.length <= 0 && (
          <div className="product__empty">Нет товаров</div>
        )}
        {productList.length > 0 && (
          <div className="product__list">
            {productList.map((product) => (
              <ProductCard
                key={product.articul}
                product={product}
                cartHandler={this.manageCart}
                isInCart={this.ckeckInCart(product.articul)}
                favoriteHandler={this.manageFavorites}
                isInFavorites={this.checkInFavorites(product.articul)}
              />
            ))}
          </div>
        )}
      </>
    );
  }
}

// const App = () => {
//   const [productList, setProductList] = useState([]);
//   useEffect(() => {
//     axios("/products.json") //запрос в файл в папку /public
//       .then((response) => {
//         setProductList(response.data);
//       })
//       .catch((err) => console.log("error", err));
//   }, []);

//   return (
//     <>
//       {productList.length <= 0 && (
//         <div className="product__empty">Нет товаров</div>
//       )}
//       {productList.length > 0 && (
//         <div className="product__list">
//           {productList.map((product) => (
//             <ProductCard key={product.articul} product={product} />
//           ))}
//         </div>
//       )}
//     </>
//   );
// };

export default App;
