import React, { Component } from 'react';
import './FavoriteList.scss';
import PropTypes from 'prop-types';

import Icon from '../Icon';

class FavoriteList extends Component {
  render() {
    const { counter } = this.props;
    return (
      <div className="favorite-list">
        <span className="favorite-list__text">
          You have <span className="favorite-list__counter">{counter}</span>{' '}
          {counter > 1 ? 'products' : 'product'} in favorites
        </span>
        <Icon type="Star" filled={counter ? true : false} size={'medium'} />
      </div>
    );
  }
}

FavoriteList.propTypes = {
  counter: PropTypes.number.isRequired,
};

export default FavoriteList;
