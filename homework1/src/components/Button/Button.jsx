import React, { Component } from 'react';
import './Button.scss';

class Button extends Component {
  render() {
    let { backgroundColor, text, handler } = this.props;
    return (
      <button
        className="btn"
        style={{ backgroundColor: backgroundColor }}
        onClick={() => {
          handler();
        }}
      >
        {text}
      </button>
    );
  }
}

export default Button;
