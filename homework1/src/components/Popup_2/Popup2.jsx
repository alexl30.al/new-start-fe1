import React, { PureComponent } from 'react';
import Modal from '../Modal';
import Button from '../Button';

class Popup2 extends PureComponent {
  state = {
    show: false,
  };

  modalShow = () => {
    this.setState({
      show: !this.state.show,
    });
  };

  btnOk = () => {
    alert('You Click Ok');
  };

  btnCancel = () => {
    alert('You Click Cancel');
    this.modalShow();
  };

  render() {
    return (
      <>
        <Button
          backgroundColor={'#164ad9'}
          handler={this.modalShow}
          text={'Open second modal'}
        />

        <Modal
          header={'Michael Jordan'}
          text={
            <>
              <p3>
                One of the most effectively marketed athletes of his generation,
                Jordan is known for his product endorsements. He fueled the
                success of Nike's Air Jordan sneakers, which were introduced in
                1984 and remain popular today. Jordan also starred as himself in
                the 1996 live-action animation hybrid film Space Jam and is the
                central focus of the Emmy Award-winning documentary miniseries
                The Last Dance (2020).
              </p3>
              <p>Do you want to learn more?</p>
            </>
          }
          actions={
            <>
              <Button
                backgroundColor={'#6a329f'}
                handler={this.btnOk}
                text={'Ok'}
              />
              <Button
                backgroundColor={'#008080'}
                handler={this.btnCancel}
                text={'Cancel'}
              />
            </>
          }
          closeButton
          handler={this.modalShow}
          show={this.state.show}
        />
      </>
    );
  }
}

export default Popup2;
